Description
===========

Console utility for deleting files / directories with support for the "Wastebusket" and the ability to restore deleted objects.

List of basic operations:
● Remove file / directory
● Remove all objects by regular expression in the specified subtree (directory)
● View the "Wastebusket" elements
● Manual cleaning of the basket, automatic cleaning by different policies
● Recovery files / directories from "Wastebusket"
