#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name="srm",
    version="1.0",
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
            ['srm = srm.main:main']
        }
)
